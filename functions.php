<?php
/**
 * Savoy-child Theme functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package savoy-child
 */

add_action( 'wp_enqueue_scripts', 'savoy_parent_theme_enqueue_styles' );

/**
 * Enqueue scripts and styles.
 */
function savoy_parent_theme_enqueue_styles() {
	wp_enqueue_style( 'savoy-style', get_template_directory_uri() . '/style.css' );
	wp_enqueue_style( 'savoy-child-style',
		get_stylesheet_directory_uri() . '/style.css',
		array( 'savoy-style' )
	);

}

add_filter( 'woocommerce_shipping_chosen_method', 'wdo_woocommerce_shipping_default_method', 10, 2 );
function wdo_woocommerce_shipping_default_method( $method, $available_methods ) {
    // Don't do anything if a method is already selected.
    if ( ! empty( $method ) ) { return $method;}  // in some case you may need to comment this line
   
    // Get the IDs of the methods that are available for Shipping options.
    $methods = array_keys( $available_methods );
    
    $preferences = array(
        'shipping-method-id-1', 
        'shipping-method-id-2',
    );
    
    // Check the preferred options and return it if it's available shipping option.
    foreach ( $preferences as $preference ) {
        if ( in_array( $preference, $methods ) ) {
            return $preference;
        }
    }
    return $method;
}


add_filter( 'woocommerce_default_address_fields', 'custom_override_default_checkout_fields', 10, 1 );
function custom_override_default_checkout_fields( $address_fields ) {
    $address_fields['address_2']['placeholder'] = __( '', 'woocommerce' );
    $address_fields['address_2']['label'] = __( 'Appartement, suite, unit, enz. (optioneel)', 'woocommerce' );
    $address_fields['address_2']['label_class'] = array(); // No label class
    return $address_fields;
}



add_action('woocommerce_checkout_process', 'custom_validation_process');
function custom_validation_process() 
{
    global $woocommerce;
 
	if(isset($_POST['billing_address_1']) and $_POST['billing_address_1'] != '')
	{
		if (!preg_match('/([0-9]+)/Uis', $_POST['billing_address_1']))
		{
			if(function_exists('wc_add_notice'))
				wc_add_notice( __('Oops, vergeet niet je huisnummer ACHTER je straatnaam te zetten.'), 'error' );
			else
				$woocommerce->add_error( __('Oops, vergeet niet je huisnummer ACHTER je straatnaam te zetten.') );
		}
	}
   
    if(isset($_POST['ship_to_different_address']))
    {
        if(isset($_POST['shipping_address_1']) and $_POST['shipping_address_1'] != '')
		{
			if (!preg_match('/([0-9]+)/Uis', $_POST['shipping_address_1']))
			{
				if(function_exists('wc_add_notice'))
					wc_add_notice( __('Oops, vergeet niet je huisnummer ACHTER je straatnaam te zetten.'), 'error' );
				else
					$woocommerce->add_error( __('Oops, vergeet niet je huisnummer ACHTER je straatnaam te zetten.') );
			}
		}
    }
}




function meks_which_template_is_loaded() {
	if ( is_super_admin() ) {
			global $template;
			print_r( $template );
	}
}
add_action( 'wp_footer', 'meks_which_template_is_loaded' );
