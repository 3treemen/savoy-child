��          D      l       �      �      �   	   �      �   %  �      �     �  	   �                               All All products loaded. Load More Shopping Cart Plural-Forms: nplurals=2; plural=(n != 1);
Project-Id-Version: Savoy Theme
PO-Revision-Date: 2020-10-22 13:36+0200
Language-Team: NordicMade
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.2.4
X-Poedit-Basepath: ..
X-Poedit-Flags-xgettext: --add-comments=translators:
X-Poedit-WPHeader: style.css
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;_nx_noop:3c,1,2;__ngettext_noop:1,2
Last-Translator: 
Language: nl_NL
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: *.js
X-Poedit-SearchPathExcluded-1: includes/theme-plugins
X-Poedit-SearchPathExcluded-2: includes/tgmpa
X-Poedit-SearchPathExcluded-3: includes/options
X-Poedit-SearchPathExcluded-4: includes/setup
X-Poedit-SearchPathExcluded-5: includes/visual-composer/elements
X-Poedit-SearchPathExcluded-6: includes/visual-composer/params
X-Poedit-SearchPathExcluded-7: includes/woocommerce/admin
X-Poedit-SearchPathExcluded-8: includes/woocommerce/widgets
X-Poedit-SearchPathExcluded-9: includes/visual-composer/elements-config.php
X-Poedit-SearchPathExcluded-10: includes/visual-composer/init.php
X-Poedit-SearchPathExcluded-11: woocommerce
X-Poedit-SearchPathExcluded-12: includes/visual-composer/deprecated/elements
X-Poedit-SearchPathExcluded-13: includes/woocommerce/deprecated
X-Poedit-SearchPathExcluded-14: includes/woocommerce/search-suggestions.php
X-Poedit-SearchPathExcluded-15: includes/admin-meta.php
 Alles Alle producten geladen. Laad Meer Winkelmandje 